<?php
session_start();
// comprobar los argumentos recibidos
if ($_POST["username"]=="prueba" && $_POST["password"]=="1234"){
    // usuario validado correctamente.
    if (array_key_exists('remember',$_POST)) {
        // Crear un nuevo cookie de sesion, que expira a los 30 días
        ini_set('session.cookie_lifetime', 60 * 60 * 24 * 30);
        session_regenerate_id(TRUE);
    }
    // Meter en sesión y enviar a página de inicio
    $_SESSION["usuario"] = $_POST["username"];
    header ("Location: index.php");
}else {
    //usuario o contraseña incorrectos.
    // Presentar mensaje de error
?>
<html>
<body>
Error. Usuario o contraseña incorrectos
<a href="/index.php">Intentarlo de nuevo</a>
</body>
</html>
<?php
}
?>