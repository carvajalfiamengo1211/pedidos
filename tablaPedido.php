<?php
require_once 'php/pedido.entidad.php';
require_once 'php/pedido.model.php';
require_once 'php/combo_medicamento.php';

$alm = new Pedidos();
$model = new PedidosModel();


if(isset($_REQUEST['action']))
{
	switch($_REQUEST['action'])
	{
    case 'InsertarImagen':
      $alm->__SET('adjunto',             $_REQUEST['adjunto']);
			$model->InsertarImagen($alm);
			//header('Location: index.php');
			break;

    case 'actualizarActivo':
			$alm->__SET('idPedido',               $_REQUEST['idPedido']);
      $alm->__SET('fecha',                  $_REQUEST['fecha']);
      $alm->__SET('detalle',                $_REQUEST['detalle']);
      $alm->__SET('estatus',                $_REQUEST['estatus']);
      $alm->__SET('activo',                 $_REQUEST['activo']);
      $alm->__SET('adjunto',                $_REQUEST['adjunto']);
      $alm->__SET('idDoctor',               $a);
      //$alm->__SET('idPedido',               $_REQUEST['idPedido']);
      $alm->__SET('idMedicamento',          $_REQUEST['idMedicamento']);
      $alm->__SET('cantidad',               $_REQUEST['cantidad']);


			$model->actualizarActivo($alm);
			header('Location: index.php');
			break;

      
    
    case 'registrar':
      $alm->__SET('nombre_doctor',          $_REQUEST['nombre_doctor']);
      $alm->__SET('fecha',                  $_REQUEST['fecha']);
      $alm->__SET('cantidad_pedido',        $_REQUEST['cantidad_pedido']);
      $alm->__SET('medicamento',            $_REQUEST['medicamento']);
      $alm->__SET('detalle',                $_REQUEST['detalle']);
      $alm->__SET('estatus',                $_REQUEST['estatus']);
      $alm->__SET('activo',                 $_REQUEST['activo']);
      $alm->__SET('adjunto',             $_REQUEST['adjunto']);

			$model->Registrar($alm);
			header('Location: index.php');
			break;

		case 'eliminar':
			$model->Eliminar($_REQUEST['id']);
			header('Location: index.php');
			break;

		case 'editar':
			$alm = $model->Obtener($_REQUEST['idPedido']);
      break;

    case 'listarNew':
      $alm = $model->ListarDetalle($_REQUEST['idPedido']);
      break;
	}
}

?>

<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<link rel="icon" type="image/png" href="imagenes/favicon.png">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"-->
<link rel="stylesheet" href="css/style.css">
<title>Gestion de Pedidos</title>
 
 
</head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.php">
            <img src="imagenes/logo.png">
          </a>
        </div>
      </div>
    </nav>
    <div class="container">
      <button> <a href="includes/logout.php">Cerrar sesión</a></button>
      <button><a href="php/cambio.php">Cambiar contraseña</a></button>
      <button><a style="color: red" href="vistas/pedidos.php">Nuevo Pedido</a></button> 
       
       <?php if(!isset($_SESSION)) 
        { 
          
          session_start(); 
          
        } 
        /*$host = "localhost";
        $puerto = "3306";
        $usuario = "root";
        $contrasena = "";
        $baseDeDatos ="medicos";
        $tabla = "doctores";*/
        

        $host = "mysql3.servidoreswindows.net";
        $puerto = "3306";
        $usuario = "Pere_conesa";
        $contrasena = "MonestiR23@@";
        $baseDeDatos ="sottopelle";
        $tabla = "doctores";

        function Conectarse() {
          global $host, $puerto, $usuario, $contrasena, $baseDeDatos, $tabla;
            if (!($link = mysqli_connect($host.":".$puerto, $usuario, $contrasena))) { 
              echo "Error conectando a la base de datos.<br>"; 
              exit(); 
            }
            else {
              //echo "Listo, estamos conectados.<br>";
            }
            if (!mysqli_select_db($link, $baseDeDatos)) { 
              echo "Error seleccionando la base de datos.<br>"; 
              exit(); 
            }
            else {
              //echo "Obtuvimos la base de datos $baseDeDatos sin problema.<br>";
            }
            return $link; 
		    }
        $usernameSesion = $_SESSION['user']; 
        $link = Conectarse();
        $b = "";
			
        $query = "SELECT nombre_doctor FROM doctores WHERE correo = '$usernameSesion'";
        $result = mysqli_query($link, $query); 
          if($row = mysqli_fetch_array($result)) {
            $b= $row['nombre_doctor'];
		      }

      ?>
    <h4>Usuario: <?php echo $b?></h4>
    </div>
    <h1 style="text-align:center">LISTADO DE PEDIDOS</h1>
      <div class="container" style="margin-top:30px; height:250px; overflow:auto">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th style="text-align:center">ACTIVO</th>
              <th style="text-align:center">ID PEDIDO</th>
              <th style="text-align:center">PACIENTE</th>
              <th style="text-align:center">FECHA PEDIDO</th>
              <!--th>DETALLE</th-->
              <th style="text-align:center">ESTATUS</th>
              <th style="text-align:center">RECETA</th>
              <th style="text-align:center">AUTORIZACION</th>
              <th hidden>DOCTOR</th>
            </tr>
          </thead>
        <tbody>
          <?php foreach($model->Listar() as $r): ?>
            <tr>
              <td style="text-align:center"><?php if($r->__GET('activo') == 1){ echo "<input type=checkbox name='condiciones' disabled value='0' checked='checked'/>"; }?> </td>
              <td style="text-align:center"><?php if($r->__GET('activo') == 1){ echo $r->__GET('idPedido');} ?></td>
              <td style="text-align:center">
                  <?php if($r->__GET('activo')== 1){ echo $r->__GET('detalle');}  ?>
                </td>
              <td style="text-align:center"><?php $timestamp = strtotime($r->__GET('fecha')); if($r->__GET('activo') == 1){ echo date('d/m/Y', $timestamp); } ?></td>
              <!--td><?php if($r->__GET('activo') == 1) {echo $r->__GET('detalle');} ?></td-->
              <td style="text-align:center"><?php if($r->__GET('activo') == 1 && $r->__GET('estatus')==1) {echo "Nuevo";}if($r->__GET('activo') == 1 && $r->__GET('estatus')==2) {echo "En Proceso";}if($r->__GET('activo') == 1 && $r->__GET('estatus')==3) {echo "Enviado";} ?></td>
              <td style="width: 150px;text-align:center">
                <a target="blank" href="uploads/<?php echo $r->__GET('adjunto');?>"><?php if($r->__GET('activo') == 1) {echo $r->__GET('adjunto');} ?>
                </a>
              </td>
              <td style="text-align:center">
                <a target="blank" href="uploads/<?php echo $r->__GET('autorizacion');?>"><?php if($r->__GET('activo') == 1) {echo $r->__GET('autorizacion');} ?>
                </a>
              </td>
              <td>
                <?php 
                $nuevo = 1;
                $enviado = 3;
                $proceso = 2;
                if($r->estatus == $nuevo && $r->activo == "1")
                {
                  //echo "<a href='vistas/tablaEditarNew.php?action=editar&idPedido=".$r->idPedido."'>Editar</a>";
                  //echo "<a href='vistas/tablaEditarNew.php?idPedido=".$r->idPedido."'>Editar</a>";
                  echo "<a href='tabla.php?idPedido=".$r->idPedido."'>Detalles</a>";
                }
                if($r->estatus == $enviado && $r->activo == "1"){
                  //echo "<a href='vistas/tablaEditarAct.php?action=editar&idPedido=".$r->idPedido."'>Editar</a>";
                  //echo "<a href=?action=actualizarActivo&idPedido=".$r->idPedido."'>Desactivar</a>";
                  echo "<a href='tabla.php?idPedido=".$r->idPedido."'>Detalles</a>";
                } if($r->estatus == $proceso && $r->activo == "1"){
                  //echo "<a href='vistas/tablaEditarAct.php?action=editar&idPedido=".$r->idPedido."'>Editar</a>";
                  //echo "<a href=?action=actualizarActivo&idPedido=".$r->idPedido."'>Editar</a>";
                  echo "<a href='tabla.php?idPedido=".$r->idPedido."'>Detalles</a>";
                } 
                  ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!--script src="js/ocultar.js"></script-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<footer style="margin-top:30px"> 
<hr>
<a href="pdf/condiciones.pdf" target="black">Condiciones de Uso</a> | 
<a href="pdf/aviso_legal.pdf" target="black">Aviso Legal</a> 
</footer>  
</body>
</html>