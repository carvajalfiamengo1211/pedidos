<?php
  session_start();
  require_once '../php/pedido.entidad.php';
  require_once '../php/pedido.model.php';
  require_once '../php/combo_medicamento.php';
  require_once '../php/consulta.php';

  // Logica
  $alm = new Pedidos();
  $model = new PedidosModel();


  if(isset($_REQUEST['action']))
  {
    switch($_REQUEST['action'])
    {
      case 'actualizar':
        
        $alm->__SET('fecha',                  $_REQUEST['fecha']);
        $alm->__SET('detalle',                $_REQUEST['detalle']);
        $alm->__SET('estatus',                $_REQUEST['estatus']);
        
        $alm->__SET('activo',                 $_REQUEST['activo']);
        $alm->__SET('adjunto',                $_REQUEST['adjunto']);
        $alm->__SET('idDoctor',               $a);
        
        $alm->__SET('idPedido',               $_REQUEST['idPedido']);
        $alm->__SET('idMedicamento',          $_REQUEST['idMedicamento']);
        $alm->__SET('cantidad',               $_REQUEST['cantidad']);

        $model->Actualizar($alm);
        header('Location: index.php');
        break;

      case 'registrar':
        $alm->__SET('nombre_doctor',          $_REQUEST['nombre_doctor']);
        $alm->__SET('fecha',                  $_REQUEST['fecha']);
        $alm->__SET('cantidad_pedido',        $_REQUEST['cantidad_pedido']);
        $alm->__SET('medicamento',            $_REQUEST['medicamento']);
        $alm->__SET('detalle',                $_REQUEST['detalle']);
        $alm->__SET('estatus',                $_REQUEST['estatus']);
        $alm->__SET('activo',                 $_REQUEST['activo']);
        $alm->__SET('adjunto',             $_REQUEST['adjunto']);

        $model->Registrar($alm);
        header('Location: index.php');
        break;

      case 'eliminar':
        $model->Eliminar($_REQUEST['id']);
        header('Location: index.php');
        break;

      case 'editar':
        $alm = $model->Obtener($_REQUEST['idPedido']);
        break;
    }
  }
?>


<!doctype html>
  <html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="../imagenes/favicon.png">
    <link rel="stylesheet" href="../css/style.css">
    <title>Gestion de Pedidos</title>
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.php">
            <img src="../imagenes/logo.png">
          </a>
        </div>
      </div>
    </nav>
    <div class="container">
      <button><a href="../includes/logout.php">Cerrar sesión</a></button> 
      <button><a href="../index.php">Ver Pedidos</a></button>
    </div>
    <h1 style="text-align:center">EDITAR PEDIDO</h1>
  <div class="container" style="margin-top:30px">
    <form action="?action=<?php echo $alm->idPedido > 0 ? 'actualizar' : 'registrar'; ?>" method="post" style="margin-bottom:15px">
      <div class="container" style="text-align: center;">
            <div class="form-group row">
            <label>Id Pedido</label>  
            <input type="text" name="idPedido" value="<?php echo $alm->__GET('idPedido'); ?>" />


            <label>Fecha Pedido</label>  
            <input type="date" name="fecha" value="<?php echo $alm->__GET('fecha'); ?>" />
            
            
              <label >E</label>  
              <input type="text" name="estatus" value="<?php if($alm->__GET('estatus')==1) echo "Nuevo" ?>" />
            

            </div>
          </div>
      <input type="hidden" name="idPedido" value="<?php echo $alm->__GET('idPedido'); ?>" />
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Activo</label>
              <div class="col-sm-10">
                <input type="checkbox" name="activo" value="1" checked>
              </div>
          </div>
        <div class="form-group row" hidden>
          <label class="col-sm-2 col-form-label">Fecha</label>
          <div class="col-sm-10">
            <input type="date" class="form-control" name="fecha"  value="<?php echo $alm->__GET('fecha'); ?>" placeholder="Fecha de Solicitud">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Cantidad Pedido</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="cantidad" placeholder="Cantidad del Pedido en cajas" value="<?php echo $alm->__GET('cantidad'); ?>">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Medicamento</label>
            <div class="col-sm-10">
              <select class="form-control" name="idMedicamento">
                <option>Seleccione</option> <?php echo $combobit; ?>
              </select>
            </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Detalle</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="detalle" placeholder="Detalle Adicional" value="">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Status</label>
            <div class="col-sm-10">
              <select name="estatus" class="form-control" >
                <option>Seleccione</option>
                <option value="1" selected>Nuevo</option>
                <option value="2">En Proceso</option>
                <option value="3">Enviado</option>
              </select>
            </div>
        </div>
        

        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Archivo adjunto</label>
            <div class="col-sm-10">
              <input type="file" name="adjunto">
            </div>
        </div>
        <div class="container" style="text-align:center">
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
    </form>
  </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <footer> 
<hr>
<a href="../pdf/condiciones.pdf" target="black">Condiciones de Uso</a> | 
<a href="../pdf/aviso_legal.pdf" target="black">Aviso Legal</a> 
</footer>
  </body>
</html>