<?php
require_once '../php/pedido.entidad.php';
require_once '../php/pedido.model.php';
require_once '../php/combo_medicamento.php';
require_once '../php/consulta.php';

$alm = new Pedidos();
$model = new PedidosModel();

if(isset($_REQUEST['action']))
{
	switch($_REQUEST['action'])
	{
		case 'actualizar':
      //$alm->__SET('activo',                 $_REQUEST['activo']);
      $alm->__SET('cantidad',               $_REQUEST['cantidad']);
      $alm->__SET('idMedicamento',          $_REQUEST['idMedicamento']);
      $alm->__SET('idDoctor',               $a);
      $alm->__SET('idPedidoDetalle',        $_REQUEST['idPedidoDetalle']);
      $alm->__SET('idPedido',               $_REQUEST['idPedido']);
      
      $model->ActualizarDetalle($alm);
			break;

    case 'registrar':
      $alm->__SET('detalle',                $_REQUEST['detalle']);
      $alm->__SET('activo',                 $_REQUEST['activo']);
      $alm->__SET('adjunto',                $_REQUEST['adjunto']);
      $alm->__SET('idDoctor',               $a);

      $alm->__SET('idPedido',               $_REQUEST['idPedido']);
      $alm->__SET('idMedicamento',          $_REQUEST['idMedicamento']);
      $alm->__SET('cantidad',               $_REQUEST['cantidad']);
      $model->Registrar($alm);
  		header('Location: ../tabla.php');
		  break;

    case 'RegistrarDetalle':
      $alm->__SET('idPedido',               $a);
      $alm->__SET('activo',                 $_REQUEST['activo']);
      $alm->__SET('idMedicamento',          $_REQUEST['idMedicamento']);
      $alm->__SET('cantidad',               $_REQUEST['cantidad']);

      $model->RegistrarDetalle($alm);
      header('Location: ../tabla.php');
      break;

		case 'eliminar':
			$model->Eliminar($_REQUEST['id']);
			header('Location: index.php');
			break;

		case 'editar':
			$alm = $model->ObtenerDetalles($_REQUEST['idPedido'], $_REQUEST['idPedidoDetalle']);
			break;
	}
}


?>
<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" type="image/png" href="../imagenes/favicon.png">
<link rel="stylesheet" href="../css/style.css">
<title>Gestion de Medicos</title>
</head>
  <body>
  <nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="../index.php">
        <img src="../imagenes/logo.png">
      </a>
    </div>
  </div>
</nav>
<div class="container">
      <button><a href="../includes/logout.php">Cerrar sesión</a></button> 
      <button><a href="../index.php">Ver Pedidos</a></button>
    </div>
    <h1 style="text-align:center">EDITAR DETALLES</h1>
<div class="container" style="margin-top:20px">
  <br>
  <form action="?action=<?php echo $alm->idPedido > 0 ? 'actualizar' : 'actualizar'; ?>" method="post" style="margin-bottom:15px">
  <div class="form-group row">
  <input type="hidden" name="idPedido" value="<?php echo $a = $_GET['idPedido']; ?>" />
  <input type="hidden" name="idPedidoDetalle" value="<?php echo $c = $_GET['idPedidoDetalle']; ?>" />
  </div>
  <!--div class="form-group row">
    <label class="col-sm-2 col-form-label">Activo</label>
      <div class="col-sm-10">
        <input type="checkbox" name="activo" id="check1" onclick='calcular()' checked="" />
      </div>
  </div-->
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Activo</label>
      <div class="col-sm-10">
        <input type='checkbox' name='activo' id='check1' value="1" <?php if ($alm->__GET('activo') == 1) { echo "checked='checked'"; } ?> />
      </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Cantidad</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="cantidad" placeholder="Cantidad del medicamento" value="<?php echo $alm->__GET('cantidad'); ?>">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Medicamentos</label>
    <div class="col-sm-10">
      <select class="form-control" name="idMedicamento">
        <option selected value="<?php echo $alm->__GET('idMedicamento'); ?>">Seleccione</option>
        <?php echo $combobit; ?>
      </select>
    </div>
  </div>
  
  <div class="container" style="text-align:center">
    <button type="submit" class="btn btn-primary">Guardar</button>
  </div>
</form>
</div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <footer> 
<hr>
<a href="../pdf/condiciones.pdf" target="black">Condiciones de Uso</a> | 
<a href="../pdf/aviso_legal.pdf" target="black">Aviso Legal</a> 
</footer>
  </body>
</html>