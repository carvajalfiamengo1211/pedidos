<?php
require_once '../php/pedido.entidad.php';
require_once '../php/pedido.model.php';
require_once '../php/combo_medicamento.php';
require_once '../php/consulta.php';

// Logica
$alm = new Pedidos();
$model = new PedidosModel();

if(isset($_REQUEST['action']))
{
	switch($_REQUEST['action'])
	{
		case 'actualizar':
      $alm->__SET('idPedido',              $_REQUEST['idPedido']);
      $alm->__SET('fecha',                $_REQUEST['fecha']);
      $alm->__SET('detalle',              $_REQUEST['detalle']);
      $alm->__SET('dni',              $_REQUEST['dni']);
      $alm->__SET('idDoctor',             $a);
      
      $model->ActualizarCabecera($alm);
			header('Location: ../index.php');
			break;

    case 'registrar':
      $alm->__SET('detalle',                $_REQUEST['detalle']);
      $alm->__SET('activo',                 $_REQUEST['activo']);
      //$alm->__SET('adjunto',                $_REQUEST['adjunto']);
      $alm->__SET('idDoctor',               $a);

      $alm->__SET('idPedido',               $_REQUEST['idPedido']);
      $alm->__SET('idMedicamento',          $_REQUEST['idMedicamento']);
      $alm->__SET('cantidad',               $_REQUEST['cantidad']);
      $model->Registrar($alm);
  		header('Location: ../tabla.php');
		  break;

    case 'RegistrarDetalle':
      $alm->__SET('idPedido',               $a);
      $alm->__SET('activo',                 $_REQUEST['activo']);
      $alm->__SET('idMedicamento',          $_REQUEST['idMedicamento']);
      $alm->__SET('cantidad',               $_REQUEST['cantidad']);

      $model->RegistrarDetalle($alm);
      header('Location: ../tabla.php');
      break;

		case 'eliminar':
			$model->Eliminar($_REQUEST['id']);
			header('Location: index.php');
			break;

		case 'editar':
			$alm = $model->ObtenerCabecera($_REQUEST['idPedido']);
			break;
	}
}


?>
<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" type="image/png" href="../imagenes/favicon.png">
<link rel="stylesheet" href="../css/style.css">
<title>Gestion de Medicos</title>
</head>
  <body>
  <nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.php">
        <img src="../imagenes/logo.png">
      </a>
    </div>
  </div>
</nav>
<div class="container">
      <button><a href="../includes/logout.php">Cerrar sesión</a></button> 
      <button><a href="../index.php">Ver Pedidos</a></button>
    </div>
    <h1 style="text-align:center">EDITAR CABECERA</h1>
<div class="container">
  <br>
  <form action="?action=<?php echo $alm->idPedido > 0 ? 'actualizar' : 'actualizar'; ?>" method="post" style="margin-bottom:30px">
  <div class="form-group row">
  <input type="hidden" name="idPedido" value="<?php echo $a = $_GET['idPedido']; ?>" />
  
  </div>
   <script type="text/javascript">
    </script>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Activo</label>
      <div class="col-sm-10">
        <input type='checkbox' name='activo' id='check1' value="1" <?php if ($alm->__GET('activo') == 1) { echo "checked='checked'"; } ?> />
      </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Fecha</label>
    <div class="col-sm-10">
      <input type="date" class="form-control" name="fecha" placeholder="Indique la fecha" value="<?php echo $alm->__GET('fecha'); ?>">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Detalle</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="detalle" value="<?php echo $alm->__GET('detalle'); ?>">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">DNI</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="dni" value="<?php echo $alm->__GET('dni'); ?>">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Estatus</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="estatus" value="<?php if($alm->__GET('estatus')==1) echo "Nuevo"; ?>" disabled>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Receta</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" value="<?php echo $alm->__GET('adjunto'); ?>" disabled>
    <!--input type="FILE" name="adjunto"/-->
    <a href="imagen.php?idPedido=<?php echo $a = $_GET['idPedido']; ?>">Actualizar Receta</a>
    </div>
    
  </div>

  <div class="form-group row">
    <label class="col-sm-2 col-form-label">Autorizacion</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" value="<?php echo $alm->__GET('autorizacion'); ?>" disabled>
    <!--input type="FILE" name="adjunto"/-->
    <a href="autorizacion.php?idPedido=<?php echo $a = $_GET['idPedido']; ?>">Actualizar Autorizacion</a>
    </div>
    
  </div>
  
  <div class="container" style="text-align:center">
    <button type="submit" class="btn btn-primary">Guardar</button>
  </div>
</form>
</div>





<footer style="margin-top:30px">
<hr>
<a href="../pdf/condiciones.pdf" target="black">Condiciones de Uso</a> | 
<a href="../pdf/aviso_legal.pdf" target="black">Aviso Legal</a> 
</footer>
    
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>