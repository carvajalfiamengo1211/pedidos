<?php 
	require_once 'php/pedido.entidad.php';
	require_once 'php/pedido.model.php';
	require_once 'php/combo_medicamento.php';
	//require_once 'php/consulta.php';

	//$conexion=mysqli_connect('localhost','root','','medicos');
	$conexion=mysqli_connect('mysql3.servidoreswindows.net','Pere_conesa','MonestiR23@@','sottopelle');
	$a= $_GET['idPedido'];

	$alm = new Pedidos();
	$model = new PedidosModel();

	if(isset($_REQUEST['action'])) {
		switch($_REQUEST['action']) {
		case 'actualizar':
		$alm->__SET('activo',                 $_REQUEST['activo']);
		$alm->__SET('detalle',                $_REQUEST['detalle']);
		$alm->__SET('cantidad',               $_REQUEST['cantidad']);
		$alm->__SET('idMedicamento',          $_REQUEST['idMedicamento']);
		$alm->__SET('estatus',                $_REQUEST['estatus']);
		$alm->__SET('adjunto',                $_REQUEST['adjunto']);
		$alm->__SET('idDoctor',               $a);
		$alm->__SET('idPedido',               $_REQUEST['idPedido']);
		
		$model->Actualizar($alm);
		header('Location: tablaEditarNew.php');
		break;
		
		case 'registrar':
		$alm->__SET('detalle',                $_REQUEST['detalle']);
		$alm->__SET('activo',                 $_REQUEST['activo']);
		$alm->__SET('adjunto',                $_REQUEST['adjunto']);
		$alm->__SET('idDoctor',               $a);

		$alm->__SET('idPedido',               $_REQUEST['idPedido']);
		$alm->__SET('idMedicamento',          $_REQUEST['idMedicamento']);
		$alm->__SET('cantidad',               $_REQUEST['cantidad']);
		$model->Registrar($alm);
		header('Location: tablaEditarNew.php');
		break;

		case 'RegistrarDetalle':
		$alm->__SET('idPedido',               $a);
		$alm->__SET('activo',                 $_REQUEST['activo']);
		$alm->__SET('idMedicamento',          $_REQUEST['idMedicamento']);
		$alm->__SET('cantidad',               $_REQUEST['cantidad']);
		$model->RegistrarDetalle($alm);
		header('Location: ../tabla.php');
		break;

		case 'eliminar':
		$model->Eliminar($_REQUEST['id']);
		header('Location: index.php');
		break;

		case 'editar':
		$alm = $model->ObtenerDetalles($_REQUEST['idPedido']);
		break;
	}
	}
?>

	<!DOCTYPE html>
	<html lang="es">
	<head>
	<meta charset="utf-8">
	<link rel="icon" type="image/png" href="imagenes/favicon.png">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/style.css">
	<title>Gestion de Pedidos</title>
	</head>
	<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="index.php">
					<img src="imagenes/logo.png">
				</a>
			</div>
		</div>
    </nav>
	<div class="container">
		<button> <a href="includes/logout.php">Cerrar sesión</a></button>
		<button><a href="index.php">Ver Pedidos</a></button>
		<button><?php echo "<a href='vistas/tablaAgregarDet.php?idPedido=".$a."'>Agregar Detalle </a>";?></button>
    
		<?php if(!isset($_SESSION)){ 
			session_start(); 
    	} 
        /*$host = "localhost";
        $puerto = "3306";
        $usuario = "root";
        $contrasena = "";
        $baseDeDatos ="medicos";
        $tabla = "doctores";*/
        
        $host = "mysql3.servidoreswindows.net";
        $puerto = "3306";
        $usuario = "Pere_conesa";
        $contrasena = "MonestiR23@@";
        $baseDeDatos ="sottopelle";
        $tabla = "doctores";

        function Conectarse() {
			global $host, $puerto, $usuario, $contrasena, $baseDeDatos, $tabla;
			if (!($link = mysqli_connect($host.":".$puerto, $usuario, $contrasena))) {
				echo "Error conectando a la base de datos.<br>"; 
				exit(); 
			}
            else {
              //echo "Listo, estamos conectados.<br>";
            }
            if (!mysqli_select_db($link, $baseDeDatos)) { 
              echo "Error seleccionando la base de datos.<br>"; 
              exit(); 
            }
            else {
              //echo "Obtuvimos la base de datos $baseDeDatos sin problema.<br>";
            }
            return $link; 
		}
        $usernameSesion = $_SESSION['user']; 
        $link = Conectarse();
        $b = "";
			
        $query = "SELECT nombre_doctor FROM doctores WHERE correo = '$usernameSesion'";
        $result = mysqli_query($link, $query); 
          if($row = mysqli_fetch_array($result)) {
            $b= $row['nombre_doctor'];
		      }

      ?>
      
    <h4>Usuario: <?php echo $b?></h4>
    <h1 style="text-align:center">Detalles del Pedido</h1>

    <table class="table table-bordered">
    	<tr >
    		<th style="text-align: center;">Id Pedido</th>
    		<th style="text-align: center;">Fecha</th>
			<th style="text-align: center;">Estatus</th>
			<th style="text-align: center;">Paciente</th>
			<th style="text-align: center;">DNI</th>
		</tr>
		<?php 
		$a= $_GET['idPedido']; //echo $a;
		$sql = "SELECT * FROM pedidos as p INNER JOIN estatus AS e ON p.estatus = e.idEstatus WHERE p.idPedido = $a";
		$result=mysqli_query($conexion,$sql);

		while($mostrar=mysqli_fetch_array($result)){
		 ?>

		<tr style="text-align: center;">
			<td ><?php if ($mostrar['activo']==1) {echo $mostrar['idPedido'];}?></td>
			<td><?php $timestamp = strtotime($mostrar['fecha']);if($mostrar['activo'] == 1){ echo date('d/m/Y', $timestamp); } ?></td>
			<td><?php if ($mostrar['activo']==1){echo $mostrar['nombre'];}?></td>
			<td><?php if ($mostrar['activo']==1){echo $mostrar['detalle'];}?></td>
			<td><?php if ($mostrar['activo']==1){echo $mostrar['DNI'];}?></td>
			<td><?php if ($mostrar['activo']==1&&$mostrar['estatus']==1){echo "<a href='vistas/tablaEditarCabecera.php?action=editar&idPedido=".$a."'>Editar</a>";} ?></td>
		</tr>	
	<?php 
	}
	 ?>
    		</table>
	<table class="table table-bordered"  >
		<tr>
			<th style="text-align:center">activo</th>
			<th style="text-align:center">Id Detalle</th>
			<!--th>Id Pedido</th-->
			<th style="text-align:center">Medicamento</th>
			<th style="text-align:center">Cantidad</th>	
		</tr>

		<?php 
		$a= $_GET['idPedido']; //echo $a;
		$sql = "SELECT * FROM pedidodetalle as pd INNER JOIN pedidos AS p ON pd.idPedido = p.idPedido INNER JOIN medicamentos AS m ON m.idMedicamento = pd.idMedicamento WHERE pd.idPedido = $a";
		$result=mysqli_query($conexion,$sql);

		while($mostrar=mysqli_fetch_array($result)){
		 ?>

		<tr>
      <td style ="width: 80px;text-align:center"><?php if ($mostrar['activo']==1) {echo "<input type=checkbox name='condiciones' disabled checked='checked'/>";}?> 
      </td>
      <? $a = $mostrar['activo'];?>
			<td style ="width: 100px;text-align:center"><?php if ($mostrar['activo']==1){echo $mostrar['idPedidoDetalle'];}?></td>
			<!--td><?php if ($mostrar['activo']==1){echo $mostrar['idPedido'];} ?></td-->
			<td style="text-align:center"><?php if ($mostrar['activo']==1){echo $mostrar['nombre'];}?></td>
			<td style="text-align:center"><?php if ($mostrar['activo']==1){echo $mostrar['cantidad'];}?></td>
			<td style="text-align:center"><?php if ($mostrar['activo']==1&&$mostrar['estatus']==1) { echo "<a href='vistas/tablaEditarNew.php?action=editar&idPedido=".$a."&idPedidoDetalle=".$mostrar['idPedidoDetalle']."'>Editar</a>";} ?></td>
		</tr>
	<?php 
	}
	 ?>
	</table>
	
</div>
</body>
</html>