<?php
class PedidosModel
{
	private $pdo;

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = new PDO('mysql:host=mysql3.servidoreswindows.net;dbname=sottopelle','Pere_conesa','MonestiR23@@');
			//$this->pdo = new PDO('mysql:host=localhost;dbname=medicos','root','');
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function ListarDetalle($id)
	{
		try
		{
			$result = array();
			
			$stm = $this->pdo->prepare("SELECT * FROM pedidodetalle as pd INNER JOIN pedidos AS p ON pd.idPedido = p.idPedido INNER JOIN medicamentos AS m ON m.idMedicamento = pd.idMedicamento WHERE pd.idPedido = ?");
			$stm->execute();

			foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r)
			{
				$alm = new Pedidos();
				$alm->__SET('idPedidoDetalle', $r->idPedidoDetalle);
				$alm->__SET('idPedido', $r->idPedido);
				$alm->__SET('activo', $r->activo);
				$alm->__SET('idMedicamento', $r->idMedicamento);
				$alm->__SET('cantidad', $r->cantidad);
				$alm->__SET('fecha', $r->fecha);
				$alm->__SET('detalle', $r->detalle);
				$alm->__SET('idDoctor', $r->idDoctor);

				$result[] = $alm;
			}
			return $result;

		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	
	public function Listar()
	{
		if(!isset($_SESSION)) 
		{ 
			session_start(); 
		} 
		$usernameSesion = $_SESSION['user'];
		try
		{
			$result = array();
			
			$stm = $this->pdo->prepare("SELECT pedidos.idPedido,pedidos.fecha,pedidos.detalle,pedidos.estatus,pedidos.activo,pedidos.adjunto, pedidos.ruta, pedidos.autorizacion, doctores.idDoctor FROM pedidos as pedidos INNER JOIN doctores ON pedidos.idDoctor = doctores.idDoctor WHERE doctores.correo = '$usernameSesion' ORDER BY pedidos.idPedido DESC");
			$stm->execute();

			foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r)
			{
				$alm = new Pedidos();
				$alm->__SET('idPedido', $r->idPedido);
				$alm->__SET('fecha', $r->fecha);
				$alm->__SET('detalle', $r->detalle);
				$alm->__SET('estatus', $r->estatus);
				$alm->__SET('activo', $r->activo);
				$alm->__SET('adjunto', $r->adjunto);
				$alm->__SET('autorizacion', $r->autorizacion);
				$alm->__SET('idDoctor', $r->idDoctor);

				$result[] = $alm;
			}

			return $result;
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Obtener($id)
	{
		try 
		{
			$stm = $this->pdo
				->prepare("SELECT Pedido.idPedido, Pedido.fecha, Pedido.detalle, Pedido.estatus, Pedido.activo, Pedido.adjunto, pDetalle.cantidad,pDetalle.idMedicamento 
				FROM pedidos AS Pedido INNER JOIN pedidodetalle AS pDetalle ON Pedido.idPedido = pDetalle.idPedidoDetalle WHERE Pedido.idPedido = ?");

				$stm->execute(array($id));
				$r = $stm->fetch(PDO::FETCH_OBJ);

				$alm = new Pedidos();
				
				$alm->__SET('idPedido', $r->idPedido);
				$alm->__SET('fecha', $r->fecha);
				$alm->__SET('cantidad', $r->cantidad);
				$alm->__SET('detalle', $r->detalle);
				$alm->__SET('estatus', $r->estatus);
				$alm->__SET('activo', $r->activo);
				$alm->__SET('idMedicamento', $r->idMedicamento);
				$alm->__SET('adjunto', $r->adjunto);

			return $alm;
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function ObtenerCabecera($id)
	{
		try 
		{
			$stm = $this->pdo
				->prepare("SELECT * FROM pedidos WHERE pedidos.idPedido = ?");

				$stm->execute(array($id));
				$r = $stm->fetch(PDO::FETCH_OBJ);

				$alm = new Pedidos();
				
				$alm->__SET('fecha', $r->fecha);
				$alm->__SET('detalle', $r->detalle);
				$alm->__SET('dni', $r->DNI);
				$alm->__SET('estatus', $r->estatus);
				$alm->__SET('activo', $r->activo);
				$alm->__SET('adjunto', $r->adjunto);
				$alm->__SET('autorizacion', $r->autorizacion);
				$alm->__SET('idDoctor', $r->idDoctor);

			return $alm;
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function ObtenerDetalles($id, $idPedidoDetalle)
	{
		try 
		{
			$stm = $this->pdo
				->prepare("SELECT Pedido.idPedido, Pedido.fecha, Pedido.detalle, Pedido.estatus, Pedido.activo, Pedido.adjunto, pDetalle.cantidad,pDetalle.idMedicamento FROM pedidos AS Pedido INNER JOIN pedidodetalle AS pDetalle ON Pedido.idPedido = pDetalle.idPedido WHERE Pedido.idPedido = ? AND pDetalle.idPedidoDetalle = ?");

				$stm->execute(array($id, $idPedidoDetalle));
				$r = $stm->fetch(PDO::FETCH_OBJ);

				$alm = new Pedidos();
				$alm->__SET('detalle', $r->detalle);
				$alm->__SET('cantidad', $r->cantidad);
				$alm->__SET('activo', $r->activo);
				$alm->__SET('idMedicamento', $r->idMedicamento);
				$alm->__SET('idPedido', $r->idPedido);
				

			return $alm;
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($id)
	{
		try 
		{
			$stm = $this->pdo
					->prepare("DELETE FROM pedidos WHERE id = ?");

			$stm->execute(array($id));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Actualizar(Pedidos $data)
	{
		try 
		{
			$sql = "UPDATE pedidos AS pedidos SET pedidos.fecha = ?, pedidos.detalle = ?, pedidos.estatus = ?, pedidos.activo = ?, pedidos.adjunto = ?, pedidos.idDoctor = ?
				    WHERE pedidos.idPedido = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				array(
					$data->__GET('fecha'),
					$data->__GET('detalle'),
					$data->__GET('estatus'),
					$data->__GET('activo'),
					$data->__GET('adjunto'),
					$data->__GET('idDoctor'),
					$data->__GET('idPedido')
					)
				);

				$sql2 = "UPDATE pedidodetalle AS pedidodetalle SET pedidodetalle.idPedido = ?,  pedidodetalle.activo = ?, pedidodetalle.idMedicamento = ?, pedidodetalle.cantidad = ?
				    WHERE pedidodetalle.idPedidoDetalle = ?";

			$this->pdo->prepare($sql2)
			     ->execute(
				array(
					$data->__GET('idPedido'),
					$data->__GET('activo'),
					$data->__GET('idMedicamento'),
					$data->__GET('cantidad'),
					$data->__GET('idPedido')
					)
				);

		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function ActualizarDetalle(Pedidos $data)
	{
		try 
		{
			$id = $_POST['idPedido'];
			$activo = $_POST['activo'];
			if ($activo == 1) {
				$activo = 1;
			} else {
				$activo = 0;
			}	
			$sql2 = "UPDATE pedidodetalle AS pedidodetalle SET pedidodetalle.activo = $activo, pedidodetalle.idMedicamento = ?, pedidodetalle.cantidad = ? WHERE pedidodetalle.idPedidoDetalle = ?";

			$this->pdo->prepare($sql2)
			     ->execute(
				array(
					//$data->__GET('activo'),
					$data->__GET('idMedicamento'),
					$data->__GET('cantidad'),
					$data->__GET('idPedidoDetalle')
					)
				); //header('Location: ../tabla.php?idPedido='.$data->__GET('idPedido'));
				header('Location: ../tabla.php?idPedido='.$id);

		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function ActualizarCabecera(Pedidos $data)
	{
		try
		{
			/*if(isset($_FILES['adjunto'])){
				echo "Entre aqui";
				// Recibo los datos de la imagen
			$nombre_img = $_FILES['adjunto']['name'];
			$tipo = $_FILES['adjunto']['type'];
			$tamano = $_FILES['adjunto']['size'];
			 
			//Si existe imagen y tiene un tamaño correcto
			if (($nombre_img == !NULL) && ($_FILES['adjunto']['size'] <= 200000)) 
			{
			   //indicamos los formatos que permitimos subir a nuestro servidor
			   if (($_FILES["adjunto"]["type"] == "image/gif")
			   || ($_FILES["adjunto"]["type"] == "image/jpeg")
			   || ($_FILES["adjunto"]["type"] == "image/jpg")
			   || ($_FILES["adjunto"]["type"] == "image/png"))
			   {
			      // Ruta donde se guardarán las imágenes que subamos
			      $directorio = $_SERVER['DOCUMENT_ROOT'].'../uploads/';
			      // Muevo la imagen desde el directorio temporal a nuestra ruta indicada anteriormente
			      move_uploaded_file($_FILES['adjunto']['tmp_name'],$directorio.$nombre_img);
			    } 
			    else 
			    {
			       //si no cumple con el formato
			       echo "No se puede subir una imagen con ese formato ";
			    }
			} 
			else 
			{
			   //si existe la variable pero se pasa del tamaño permitido
			   if($nombre_img == !NULL) echo "La imagen es demasiado grande "; 
			}
			}*/

			$a = $_POST['idPedido'];
			$activo = $_POST['activo'];
			if ($activo == 1) {
				$activo = 1;
			} else {
				$activo = 0;
			}	
			
			/*$sql = "UPDATE pedidos AS pedidos SET pedidos.fecha = ?, pedidos.detalle = ?, pedidos.estatus = 1, pedidos.activo = $activo, pedidos.adjunto = ?, pedidos.idDoctor = ? WHERE pedidos.idPedido = ?";*/
			$sql = "UPDATE pedidos AS pedidos SET pedidos.fecha = ?, pedidos.detalle = ?, pedidos.DNI = ?, pedidos.estatus = 1, pedidos.activo = $activo, pedidos.idDoctor = ? WHERE pedidos.idPedido = ?";

			$this->pdo->prepare($sql)->execute(array(
				$data->__GET('fecha'),
				$data->__GET('detalle'),
				$data->__GET('dni'),
				$data->__GET('idDoctor'),
				$data->__GET('idPedido')
			));

		} 
			catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Registrar(Pedidos $data)
	{
		try 
		{
			//$cn = mysqli_connect("localhost","root","","medicos") or die("Error");
			$cn = mysqli_connect("mysql3.servidoreswindows.net","Pere_conesa","MonestiR23@@","sottopelle") or die("Error");
			$sql = "INSERT INTO pedidos (fecha,detalle,dni,estatus,activo,idDoctor) VALUES (now(),?,?, 1, 1, ?)";
			    	$this->pdo->prepare($sql)
			    	->execute(
			    		array(
								$data->__GET('detalle'),
								$data->__GET('dni'),
							$data->__GET('idDoctor')
						)
			    	);
			$link = Conectarse();
			$a = "";
			$query = "SELECT idPedido FROM pedidos ORDER BY idPedido DESC LIMIT 1;";
			$result = mysqli_query($link, $query); 
			if($row = mysqli_fetch_array($result)) {
				$a= $row['idPedido'];
			}
			echo $a;

			$sql1 = "INSERT INTO pedidodetalle (idPedido,activo,idMedicamento,cantidad) VALUES ($a, ?, ?, ?)";
			$this->pdo->prepare($sql1)
			->execute(
				array(
					$data->__GET('activo'), 
					$data->__GET('idMedicamento'), 
					$data->__GET('cantidad')
				)
			);

			if(isset($_FILES['autorizacion'])){
				$nombreImg=$_FILES['autorizacion']['name'];
			    $ruta=$_FILES['autorizacion']['tmp_name'];
			    $destino="../uploads/".$nombreImg;
			    if(copy($ruta,$destino)){
			    	$sql4="UPDATE pedidos SET autorizacion = '$nombreImg', rutaAutorizacion = '$ruta' WHERE idPedido = $a";
			    	$res=mysqli_query($cn,$sql4);
			    	if($res){
			    	
			        }else{
			            die("Error".mysqli_error($cn));
			        }
			   	}
			}

			if(isset($_FILES['adjunto'])){
				$nombreImg=$_FILES['adjunto']['name'];
			    $ruta=$_FILES['adjunto']['tmp_name'];
			    $destino="../uploads/".$nombreImg;
			    if(copy($ruta,$destino)){
			    	$sql3="UPDATE pedidos SET adjunto = '$nombreImg', ruta = '$ruta' WHERE idPedido = $a";
			    	$res=mysqli_query($cn,$sql3);
			    	if($res){
			    	
			        }else{
			            die("Error".mysqli_error($cn));
			        }
			   	} header('Location: ../tabla.php?idPedido='.$a);
			}
			

		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function RegistrarDetalle(Pedidos $data)
	{
		try 
		{
			$sql1 = "INSERT INTO pedidodetalle (idPedido,activo,idMedicamento,cantidad) 
			VALUES (?, ?, ?, ?)";

		$this->pdo->prepare($sql1)
			->execute(
			array(
			$data->__GET('idPedido'), 
			$data->__GET('activo'), 
			$data->__GET('idMedicamento'), 
			$data->__GET('cantidad')
			)
		); 
			
			header('Location: ../tabla.php?idPedido='.$data->__GET('idPedido'));

		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function mysql()
	{
		/*$host = "localhost";
        $puerto = "3306";
        $usuario = "root";
        $contrasena = "";
        $baseDeDatos ="medicos";
		$tabla = "doctores";*/
			

		$host = "mysql3.servidoreswindows.net";
      	$puerto = "3306";
      	$usuario = "Pere_conesa";
      	$contrasena = "MonestiR23@@";
      	$baseDeDatos ="sottopelle";
      	$tabla = "doctores";


        function Conectarse() {
          global $host, $puerto, $usuario, $contrasena, $baseDeDatos, $tabla;
            if (!($link = mysqli_connect($host.":".$puerto, $usuario, $contrasena))) { 
              echo "Error conectando a la base de datos.<br>"; 
              exit(); 
            }
            else {
              //echo "Listo, estamos conectados.<br>";
            }
            if (!mysqli_select_db($link, $baseDeDatos)) { 
              echo "Error seleccionando la base de datos.<br>"; 
              exit(); 
            }
            else {
              //echo "Obtuvimos la base de datos $baseDeDatos sin problema.<br>";
            }
            return $link; 
		  }
 	}

	public function ActualizarActivo(Pedidos $data)
	{
		try 
		{
			$sql = "UPDATE pedidos AS pedido SET pedido.activo = ? WHERE pedido.idPedido = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				array(
					$data->__GET('activo'),
					$data->__GET('idPedido')
					)
				);

				$sql2 = "UPDATE pedidodetalle AS pedidodetalle SET pedidodetalle.activo = ? WHERE pedidodetalle.idPedido = ?";

				$this->pdo->prepare($sql2)
						 ->execute(
					array(
						$data->__GET('activo'),
						$data->__GET('idPedido')
						)
					);

		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}