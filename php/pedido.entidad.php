<?php
class Pedidos
{
	private $idPedido;
	private $idDoctor;
	private $nombre_doctor;
	private $fecha;
	private $cantidad;
	private $idMedicamento;
	private $detalle;
	private $estatus;
	private $activo;
	private $adjunto;
	private $idPedidoDetalle;
	private $ruta;
	private $autorizacion;
	private $rutaAutorizacion;
	private $dni;

	public function __GET($k){ return $this->$k; }
	public function __SET($k, $v){ return $this->$k = $v; }
}