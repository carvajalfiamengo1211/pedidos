<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/png" href="../imagenes/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cambiar Contraseña</title>

    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container" style="margin-left:110px">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.php">
        <img src="../imagenes/logo.png">
      </a>
    </div>
    
  </div>
 
</nav>
<div class="container">
<button><a href="../index.php">Ver Pedidos</a></button>
</div>
<div class="container" style="text-align:center">
<!--form name="formName" action="" method="POST" onsubmit='return validate_password()'-->
<form action="update.php" method="POST" >
    <!--h2>Cambiar Contraseña</h2-->
    <div class="container" >
      <h2>Cambio de contraseña</h2>
      <p>1.- Mínimo 6 carácteres, máximo 16 carácteres.</p>
      <p>2.- Mínimo 1 carácter minúscula y 1 carácter mayúsculas.</p>
      <p>3.- Mínimo 1 carácter especial. Permitidos: <strong>@!"#$%&()*+,-./ </strong> </p>
      <p>4.- Mínimo 1 carácter numérico. Permitidos: <strong> 0 - 9</strong> </p>
      <p>5.- Su contraseña no puede coincidir con la anterior.</p>
    </div>
<div class="container"  style="margin-top:50px">
<!--label for="">Correo:</label>     <input type="text" name="correo"/-->
	<label for="">Contraseña Actual:</label>     <input type="password" name="pass"/>
	<label for="">Nueva contraseña:</label>  <input type="password" name="pass_new"/>
	<label for="">Repita contraseña:</label> <input type="password" name="pass_new2"/>
  <button type="submit" class="btn btn-primary" >Cambiar</button>
</div>

    
</form>
</div>
<footer style="margin-top:10%"> 
<hr>
<a href="../pdf/condiciones.pdf" target="black">Condiciones de Uso</a> | 
<a href="../pdf/aviso_legal.pdf" target="black">Aviso Legal</a> 
</footer>
</body>
</html>